`ksargs` is a tool for transforming shell command arguments in parallel.
It resembles a kind of shell map/reduce.

Syntax is

    ksargs filter command [options] -- arg...

where `filter` is some command to be applied to each `arg`,
and `command [options]` is the shell command to apply to the results.

The most likely use is when each arg is the name of a compressed file,
in which case `filter` would decompress and `command [options]` would combine the results.
For example

    ksargs zcat sort -m -- clients-05??.gz

unzips each of `clients-05??.gz`and applies `sort -m` to merge the collection.
In this case, simply catenating the unzipped files with `zcat clients-05??.gz` wouldn't allow a merge.

# Implementation

`ksargs` effectively runs

    command [options] <(filt arg)...

by creating a pipe and forking a subprocess for each `arg` to write to the pipe,
then running the command with `/dev/fd/nn`s to read from the pipes.

## Limitations

Unlike parallel tools such as `xargs` or GNU `parallel`,
`ksargs` forks subprocesses for _all_ `arg`s at once.

The `filter` is restricted to a single command without options,
although this could invoke a more complex shell script.
