#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/* suppress logging */
int quiet = 0;

/* describe process */
static void
pargs(int pid, char *argv[]) {
	fprintf(stderr, "[%d]", pid);
	for (int a = 0; argv[a]; a++) {
		fprintf(stderr, "%c%s", (a ? ' ' : '\t'), argv[a]);
	}
	fprintf(stderr, "\n");
}

/* fork/exec a subprocess with a pipe to read from */
static int
pipefork(char *argv[]) {
	/* pipe ends */
	enum { R=0, W = 1};
	int pfd[2];
	int pid;
	int err = pipe(pfd);
	if (err) {
		perror("pipe fail");
		exit(-1);
	}
	pid = fork();
	if (!pid) {
		/* child pipes from forked STDOUT */
		err = dup2(pfd[W], STDOUT_FILENO);
		close(pfd[R]);
		execvp(argv[0], argv);
		perror(argv[0]);
		exit(-1);
	}
	else {
		/* parent reads from pipe */
		if (!quiet) {
			fprintf(stderr, "%d <", pfd[R]);
			pargs(pid, argv);
		}
		close(pfd[W]);
	}
	return pfd[R];
}

static void
usage(char *name) {
	fprintf(stderr, "%s filter command [options] -- arg...\n", name);
	exit(-1);
}

int
main(int argc, char *argv[]) {
	char **execv = calloc(argc - 1, sizeof *execv);
	char *filtv[3] = {};
	int a;
	if (argc < 3) {
		fprintf(stderr, "Apply filter to each arg before passing to command.\n");
		usage(argv[0]);
	}
	/* collect command & options */
	execv[0] = NULL;
	for (a = 2; a < argc; a++) {
		if (!strcmp(argv[a], "--")) {
			break;
		}
		execv[a - 2] = argv[a];
	}
	if (a == argc) {
		usage(argv[0]);
	}
	a++;
	/* filter to apply to args */
	filtv[0] = argv[1];
	/* pipe remaining args */
	while (a < argc) {
		if (!strcmp(argv[a], "--")) {
			/* allow -- in args for nested use */
			execv[a - 3] = argv[a];
		}
		else {
			/* paste arg pipe name into filter command */
			char afd[64];
			filtv[1] = argv[a];
			snprintf(afd, sizeof afd, "/dev/fd/%d", pipefork(filtv));
			execv[a - 3] = strdup(afd);
		}
		a++;
	}
	/* run command with filtered args */
	if (!quiet) {
		pargs(getpid(), execv);
	}
	execvp(execv[0], execv);
	perror("exec fail");
	exit(-1);
}
